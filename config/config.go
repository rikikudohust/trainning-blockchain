package config

type Config struct {
	Server struct {
		Port string `yaml:"port"`
		Host string `yaml:"host"`
	} `yaml:"server"`
	Chain struct {
		ListChain []int `yaml:"list_chain"`
	} `yaml:"chain"`
}

type NativeCurrency struct {
	Name    string `json:"name"`
	Symbol  string `json:"symbol"`
	Decimal int    `json:"decimal"`
}

type ChainConfig struct {
	Name           string         `json:"name"`
	ChainID        int            `json:"chainId"`
	ShortName      string         `json:"shortName"`
	NetworkId      int            `json:"networkId"`
	NativeCurrency NativeCurrency `json:"nativeCurrency"`
	RPC            []string       `json:"rpc"`
}

type JsonConfig struct {
  Chains map[string]ChainConfig `json:"chainlist"`
}
