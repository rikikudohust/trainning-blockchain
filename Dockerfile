FROM golang:1.19.4-alpine as BUILD
WORKDIR /app
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /out/main ./cmd/

FROM scratch
WORKDIR /
COPY --from=build /out/main /out/main
EXPOSE 5000
ENTRYPOINT ["/out/main"]
