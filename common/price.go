package common

import (
	"time"
)

type CurrencyPrice struct {
	Lastime      time.Time `db:"time"`
	CurrencyCode string    `db:"currency_code"`
	Price        float64   `db:"price"`
}

type AvarageCurrencyPrice struct {
	Start        time.Time `json:"start"`
	End          time.Time `json:"end"`
	AvaragePrice float64   `json:"avarage_price"`
}
