package common

import (
  "strconv"

	"github.com/ethereum/go-ethereum/ethclient"
)

type ChainID int

type ChainInformation struct {
	ChainID     int `json:"chainID"`
	LatestBlock int `json:"latestBlock"`
}

type Chain struct {
	ChainID int
	Client  *ethclient.Client
}

func NewChain(
	ChainID int,
	Client *ethclient.Client,
) *Chain {
	return &Chain{
		ChainID: ChainID,
		Client:  Client,
	}
}

func (c *Chain) ChangeClientRPC(rpc string) error {
  return nil
}

func(c *Chain) CheckRPC() bool {
  return true
}

func(cid *ChainID) ToString() string{
  return  strconv.Itoa(int(*cid))
}

func StringToChainID(cid string) (ChainID, error) {
  result, err := strconv.Atoi(cid)
  if err != nil {
    return 0, err
  } 
  return ChainID(result), nil
}
