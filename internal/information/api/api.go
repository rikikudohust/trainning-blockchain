package api

import  (
  "github.com/gin-gonic/gin"
  "gitlab.com/rikikudohust/trainning-blockchain/common"
)

type API struct {
  Version int
  Client map[int]common.Chain
}

type Config struct {
  Client map[int]common.Chain
  Server *gin.Engine
  Version int
}

func NewAPI(setup Config) (*API, error) {
  a := &API{
    Version: setup.Version,
    Client: setup.Client,
  }

  v1 := setup.Server.Group("/v1")
  v1.GET("/blocks", a.GetChainInformation)
  return a, nil
} 
