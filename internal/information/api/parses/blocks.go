package parses

import (
	"github.com/gin-gonic/gin"
	// "gitlab.com/rikikudohust/trainning-blockchain/common"
)

type ChainBlockQuery struct {
  ChainID int `form:"chainID"`
}

func ParseChainBlockQuery(c *gin.Context) (int ,error) {
  var getChainID ChainBlockQuery 
  err := c.ShouldBindQuery(&getChainID)
  if err != nil {
    return 0, err
  }
  return getChainID.ChainID, nil
}
