package api

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/rikikudohust/trainning-blockchain/common"
	"gitlab.com/rikikudohust/trainning-blockchain/internal/information/api/parses"
)

func (a *API) GetChainInformation(c *gin.Context) {
	chainID, err := parses.ParseChainBlockQuery(c)
	if err != nil {
		return
	}


	client := a.Client[chainID]

	fmt.Println(client)
	latestBlock, err := client.Client.BlockNumber(context.Background())
	if err != nil {
		return
	}

	c.JSON(http.StatusOK, common.ChainInformation{
		ChainID:     chainID,
		LatestBlock: int(latestBlock),
	})
}
