package main

import (
	// "github.com/joho/godotenv"

	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
  "io"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/gin-gonic/gin"
	"gitlab.com/rikikudohust/trainning-blockchain/common"
	"gitlab.com/rikikudohust/trainning-blockchain/internal/information/api"

	"gitlab.com/rikikudohust/trainning-blockchain/config"

	"gopkg.in/yaml.v2"
)

// This version support 3 chain
// func loadConfig() {
// }

func loadJsonConfig(cfg *config.Config) ([]config.ChainConfig, error) {
	jsonFile, err := os.Open("./config/chainRPC.json")
	if err != nil {
		return nil, err
	}

	defer jsonFile.Close()
	data, _ := io.ReadAll(jsonFile)

	var chainCfg config.JsonConfig
	err = json.Unmarshal(data, &chainCfg)
	if err != nil {
		return nil, err
	}

	listChain := cfg.Chain.ListChain
	supportChainConfig := make([]config.ChainConfig, len(listChain))

	for i, c := range listChain {
		supportChainConfig[i] = chainCfg.Chains[strconv.Itoa(c)]
	}

	return supportChainConfig, nil
}

func loadConfig() *config.Config {
	data, err := os.ReadFile("./config/config.yaml")
	if err != nil {
		panic(err)
	}

	var config config.Config
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		panic(err)
	}
	return &config
}

func main() {
	configData := loadConfig()
	configChain, err := loadJsonConfig(configData)
	if err != nil {
		fmt.Println(err)
	}

  fmt.Println(configChain)


	clients := make(map[int]common.Chain)

	for _, c := range configChain {
		newcl, err := ethclient.Dial(c.RPC[0])
		if err != nil {
			panic(err)
		}
		clients[c.ChainID] = *common.NewChain(c.ChainID, newcl)
	}
  fmt.Println(clients)

	router := gin.Default()

	setup := api.Config{
		Server:  router,
		Version: 1,
	   Client: clients,
	}

	_, err = api.NewAPI(setup)

	if err != nil {
		panic(err)
	}

  err = router.Run(":" + configData.Server.Port)
  if err != nil {
    log.Fatal(err)
  }
}
